import { AnyAction } from "redux";
import { Pages } from "../types/Pages";
import { TemplateResult } from "lit-html";
import { REGISTER_PAGE } from "../actions/app";

const initialPages = {} as {
    [pageName: string]: TemplateResult
}

export default function (state: Pages = initialPages, action: AnyAction) {
	switch (action.type) {
		case REGISTER_PAGE:
			state[action.name] = action.page;
			return state;
		default:
			return state;
	}
}