import { Playlist } from "../types/Playlist";
import { AnyAction } from "redux";
import { SET_CURRENT_PLAYLIST, UNSET_CURRENT_PLAYLIST } from "../actions/currentPlaylist";

export default function (state: Playlist = null, action: AnyAction) {
    switch (action.type) {
        case SET_CURRENT_PLAYLIST:
            return action.playlist;
        case UNSET_CURRENT_PLAYLIST:
            return null;
        default:
            return state;
    }
}