import { AnyAction } from "redux";
import { SET_PROFILE } from "../actions/profile";


export default function (state: string = "", action: AnyAction) {
   switch (action.type) {
      case SET_PROFILE:
         return action.name;
      default:
         return state;
   }
}