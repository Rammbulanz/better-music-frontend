import { AnyAction } from "redux";
import { VIEW_PAGE } from "../actions/app";

const initialView = "viewSongs";


export default function (state = initialView, action: AnyAction) {
   switch (action.type) {
		
	case VIEW_PAGE:
	   return action.view || "";
	
	default:
	   return state;
   }
}