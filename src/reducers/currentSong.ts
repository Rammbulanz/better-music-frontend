import { AnyAction } from "redux";
import { SET_CURRENT_SONG, UNSET_CURRENT_SONG } from "../actions/currentSong";
import { SongResponse } from "../types/rest-results/SongResponse";

export default function currentSong(state: SongResponse = null, action: AnyAction): SongResponse {
    switch (action.type) {
        case SET_CURRENT_SONG:
            return action.song;
        case UNSET_CURRENT_SONG:
            return null;
        default:
            return state;
    }
}