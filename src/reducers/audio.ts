import { AnyAction } from "redux";
import { SET_CURRENT_SONG, UNSET_CURRENT_SONG } from "../actions/currentSong";
import { AUDIO_PLAY, AUDIO_PAUSE } from "../actions/audio";

export default function (state = document.createElement("audio"), action: AnyAction) {
	switch (action.type) {

		case SET_CURRENT_SONG:
			state.src = "/btm/music/stream?id=" + action.song.id;
			state.play();
			return state;

		case UNSET_CURRENT_SONG:
			state.src = "";
			return state;

		case AUDIO_PLAY:
			if (state.src)
				state.play();
			return state;

		case AUDIO_PAUSE:
			state.pause();
			return state;

		default:
			return state;

	}
}