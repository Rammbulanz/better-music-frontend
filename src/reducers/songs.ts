import { AnyAction } from "redux";
import { ADD_SONG, SET_LIBRARY } from "../actions/library";
import { SongResponse } from "../types/rest-results/SongResponse";

export default function (state: SongResponse[] = [], action: AnyAction) {
   switch (action.type) {
      case ADD_SONG:
         state.push(action.song);
         return state;

      case SET_LIBRARY:
         state = [];

         for (let artist in action.library) {
            for (let album in action.library[artist]) {
               state = [...state, ...action.library[artist][album]];
            }
         }

         return state;

      default: 
         return state;
   }
}