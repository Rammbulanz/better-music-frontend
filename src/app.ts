import './index'
import { render } from 'lit-html'
import { html } from '@polymer/lit-element'

render(html`<app-component></app-component>`, document.getElementById("root"));


// Add an event to window that should prevent space bar from triggering a scroll
window.onkeydown = function (e: any) {
    e = e || window.event;  //normalize the evebnt for IE
    var target = e.srcElement || e.target;  //Get the element that event was triggered on
    var tagName = target.tagName;  //get the tag name of the element [could also just compare elements]
    return !(tagName === "BODY" && e.keyCode == 32);  //see if it was body and space
};