export function createResponseErrorMesage(res: Response) {
   return `${res.url}: ${res.status} - ${res.statusText}`;
}
