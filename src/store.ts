import { createStore, combineReducers } from 'redux'
import currentSong from "./reducers/currentSong";
import currentPlaylist from './reducers/currentPlaylist'
import profile from './reducers/profile'
import lastAction from './reducers/lastAction'
import audio from './reducers/audio'
import view from './reducers/view'
import pages from './reducers/pages'

export const store = createStore(combineReducers({
   view,
   pages,
   audio,
   profile,
   currentSong,
   currentPlaylist,
   lastAction
}));

// While debugging, keep an version of state on window
store.subscribe(() => {
   (window as any).state = store.getState();
});