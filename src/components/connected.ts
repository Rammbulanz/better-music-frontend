import { LitElement } from "@polymer/lit-element";
import { store } from "../store";
import { connect } from 'pwa-helpers'

export const Connected = connect(store)(LitElement);