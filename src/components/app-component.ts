import { html, property } from '@polymer/lit-element'
import { TStoreState } from '../types/TStoreState';
import { VIEW_PAGE } from '../actions/app';
import { store } from '../store';
import { Connected } from './connected';
import { _default, scrollbar } from './shared-styles';
import { TemplateResult } from 'lit-html';



export class AppComponent extends Connected {

    @property({ type: String })
    public view: string = "albumCollection";

    @property({ type: Object })
    public generatedView: TemplateResult

    public get drawer() {
        return this.shadowRoot.querySelector("#drawer") as HTMLElement & {
            toggle: () => void
        };
    }

    protected render() {
        return html`
            ${_default}
            ${scrollbar}

            <style>
                :host {
                    position: relative;
                    display:flex;
                    flex-direction: column;
                    width: 100%;
                    height: 100%;
                    justify-content: stretch;
                    padding: 0;
                }

                #pages {
                    overflow-y: auto;
                    padding: 15px;
                    flex-grow: 1;
                    position: relative;
                }
                #pages > * {
                    position: absolute;
                    left: 0;
                    top: 0;
                    right: 0;
                    bottom: 0;
                }

                paper-tabs {
                    --paper-tabs-selection-bar-color: blue;
                }
                paper-tab {
                    --paper-tab-ink: blue;
                }

            </style>
        
            <paper-tabs>
                <paper-tab @click="${() => this._setView("currentPlayback")}">Aktuelle Wiedergabe</paper-tab>
                <paper-tab @click="${() => this._setView("playlists")}">Playlists</paper-tab>
                <paper-tab @click="${() => this._setView("viewArtists")}">Interpreten</paper-tab>
                <paper-tab @click="${() => this._setView("viewAlbumCollection")}">Alben</paper-tab>
                <paper-tab @click="${() => this._setView("viewSongs")}">Titel</paper-tab>
            </paper-tabs>
            
            <iron-pages id="pages" selected="${this.view}" attr-for-selected="page-name">
                <view-current-playback page-name="currentPlayback"></view-current-playback>
                <view-album-collection page-name="viewAlbumCollection" src="/btm/music/list/albums"></view-album-collection>
                <view-artists page-name="viewArtists" src="/btm/music/list/artists"></view-artists>
                <lit-songlist page-name="viewSongs" src="/btm/music/list/songs" mainTitle="Songs"></lit-songlist>
                ${this.generatedView}
            </iron-pages>
            
            <div>
                <lit-audio></lit-audio>
            </div>

        `;
    }

    private _setView(view: string) {
        this.view = view;
    }

    private _selectedViewChanged(e: CustomEvent) {
        store.dispatch({
            type: VIEW_PAGE,
            view: e.detail.value
        });
    }

    _stateChanged(state: TStoreState) {
        if (state && state.lastAction) {
        }
    }


}

window.customElements.define("app-component", AppComponent);



declare global {
    interface HTMLElementTagNameMap {
        "app-component": AppComponent
    }
}

