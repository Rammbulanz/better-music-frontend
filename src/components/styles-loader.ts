import { html } from '@polymer/lit-element'
import { TemplateResult } from 'lit-html';

const _styles = {}  as {
   [url: string]: Promise<TemplateResult>
};

export async function LoadCssFile(url: string) {

   if (!_styles[url]) {
      _styles[url] = fetchCssFile(url);
   }
     
   return _styles[url];
}
export async function fetchCssFile(url: string) {

   const res = await fetch(url);

   if (res.status == 200) {

      const cssText = await res.text();
      return html`<style>${cssText}</style>`;

   } else {
      throw new Error(`${res.url}: ${res.status} - ${res.statusText}`);
   }

}

export async function LoadCssFiles(urls: string[]) {
   return await Promise.all(urls.map(url => LoadCssFile(url)));
}