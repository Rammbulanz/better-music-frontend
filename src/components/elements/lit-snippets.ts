import { html } from '@polymer/lit-element'

export const img = (src: string, fallbackSrc: string, id = "", className = "") => html`
    <img 
        id="${id}"
        class="${className}"
        src="${src}"
        onerror="${(e) => { if (e.target.src != fallbackSrc) e.target.src = fallbackSrc; }}"
        />
`;