import { LitElement, html, property, PropertyValues } from "@polymer/lit-element";
import { setCurrentPlaylist } from "../../dispatcher/currentPlaylist";
import { setCurrentSong } from "../../dispatcher/currentSong";
import { SongResponse } from "../../types/rest-results/SongResponse";
import {_default, card, table} from "../shared-styles";


class SongList extends LitElement {

	@property({ type: String })
	public src: string

	@property({ type: String })
	public mainTitle: string;

	@property({ type: String })
    public subTitle: string;

	@property({ type: Array })
	public songs: Array<SongResponse>;

	_renderSong(index: number, song: SongResponse) {
		return html`
			<paper-item two-line @click="${() => this._songRowClicked(song)}">
				${index}
				<paper-item-body>
					<div>${song.title}</div>
					<div secondary>
						<span>${song.album}</span>
						-
						<span>${song.artist}</span>
					</div>
				</paper-item-body>
				<paper-icon-button icon="menu"></paper-icon-button>
			</paper-item>
		`;
	}

	protected update(changed: PropertyValues) {

		if (changed.has("src")) {
			this.loadSongList(this.src as string);
		}

		return super.update(changed);
	}

	protected render() {

		if (!this.songs) {
			return html``;
		}

		return html`

			${_default}
			${card}

			<style>
				:host {
					padding: 0px 20px;
				}
				.header {
					padding-left: 25px;
					padding-bottom: 25px;
				}
				.title {
					font-weight: bold;
					font-size: 1.3em;
				}
				.subTitle {
					font-size: 0.85em;
				}
					
				
				.song-list-container {
					background-color: white;
					border-radius: 2px;
					margin: 24px 8px 0;
					box-shadow: 0 1px 4px 0 rgba(0,0,0,0.37);
					transition: box-shadow 0.28s cubic-bezier(0.4,0,0.2,1);
				}

				.song-cover {
					width: 48px;
					height: 48px;
				}

				paper-item-body {
					margin-left: 10px;
				}

			</style>
			

			<div class="header">
				<div class="title">${this.mainTitle}</div>
				<div class="subTitle">${this.subTitle}</div>
			</div>
			
			<div class="song-list-container">
				<paper-listbox>
					${this.songs.map((song, index) => this._renderSong(index, song))}
				</paper-listbox>
			</div>
			
		`;
	}

	public async loadSongList(url: string) {
		if(!url) {
			return;
		}

		const res = await fetch(url);

		if (res.status == 200) {
			this.songs = await res.json();
		} else {
			console.error(res.url, res.status, res.statusText);
		}
	}

	_songRowClicked(song: SongResponse) {
		this._startPlaying(song);
	}

	_startPlaying(song: SongResponse) {
		setCurrentPlaylist({
			current: 0,
			songs: this.songs,
			title: this.title
		});
		setCurrentSong(song);
	}

}

window.customElements.define("lit-songlist", SongList);


declare global {
	interface HTMLElementTagNameMap {
		"lit-songlist": SongList;
	}
}