import { html } from 'lit-html'

/**
 * 
 * @param duration Time in seconds
 */
export const formatDuration = (duration: number) => {
   const minutes = Math.floor(duration / 60);
   const seconds = duration % 60;

   return html`<span>${String(minutes)}:${String(seconds)}</span>`;
}