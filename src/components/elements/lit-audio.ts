import { html, property } from "@polymer/lit-element";
import { Connected } from "../connected";
import { TStoreState } from "../../types/TStoreState";
import { store } from "../../store";
import { AUDIO_PLAY, AUDIO_PAUSE } from "../../actions/audio";
import { _default } from "../shared-styles";

export type TAudioProps = {
    songTitle: string
    album: string
    artist: string
    url: string
    isPlaying: boolean
};

class LitAudio extends Connected implements TAudioProps {

    @property({ type: String })
    public songTitle: string = ""

    @property({ type: String })
    public album: string = ""

    @property({ type: String })
    public artist: string = ""

    @property({ type: String })
    public url: string = ""

    @property({ type: Boolean })
    public isPlaying = false

    @property({ type: Boolean })
    public opened = false


    private _onKeyUp: (e: KeyboardEvent) => void;
    private _set_playing_true = () => this.isPlaying = true;
    private _set_playing_false = () => this.isPlaying = false;
    private _update_time = () => this.updateProgress();


    public get player() {
        return store.getState().audio;
    }

    public get progressbar() {
        return this.shadowRoot
            ? this.shadowRoot.querySelector("#progress") as HTMLInputElement
            : null;
    }

    connectedCallback() {
        super.connectedCallback();

        if (!this._onKeyUp) {
            this._onKeyUp = (e) => this.onKeyUp(e);
        }

        window.addEventListener("keyup", this._onKeyUp);

        // Add audio events
        const audio = store.getState().audio;
        audio.addEventListener("play", () => this._set_playing_true);
        audio.addEventListener("playing", this._set_playing_true);
        audio.addEventListener("pause", this._set_playing_false);
        audio.addEventListener("abort", this._set_playing_false);
        audio.addEventListener("ended", this._set_playing_false);
        audio.addEventListener("timeupdate", this._update_time);


    }
    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener("keyup", this._onKeyUp);

        // Add audio events
        const audio = store.getState().audio;
        audio.removeEventListener("play", () => this._set_playing_true);
        audio.removeEventListener("playing", this._set_playing_true);
        audio.removeEventListener("pause", this._set_playing_false);
        audio.removeEventListener("abort", this._set_playing_false);
        audio.removeEventListener("ended", this._set_playing_false);
        audio.removeEventListener("timeupdate", this._update_time);

    }

    protected render() {

        return html`
            ${_default}
            <style>
                :host {
                    position: relative;
                    display:flex;
                    justify-content: space-between;
                    align-items: center;
                    padding: 0px;
                    box-shadow: 0 0 8px rgba(0,0,0,.4);
                }

                #songInfo, #mediaControls, #moreActions {
                    flex: 1;
                    display:flex;
                    position: relative;
                }

                #mediaControls {
                    justify-content: left;
                }
                #songInfo {
                    display:flex; 
                    align-items: center;
                    justify-content: center;
                }
                #moreActions {
                    justify-content: flex-end;
                }

                #imageWrapper {
                    position: relative;
                    float: left;
                    border: 0;
                    padding: 0;
                    margin: 0 10px 0 0;
                    height: 70px;
                    width: 70px;
                }
                #playerBarArt {
                    display: block;
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                }

                .link:hover {
                    text-decoration: underline;
                }

                #progress-wrapper {
                    position: absolute;
                    left: 0px;
                    top: -15px;
                    right: 0px;
                }
            </style>
            
            <div id="progress-wrapper">
                <vaadin-progress-bar id="progress"></vaadin-progress-bar>
            </div>
            
            <div id="mediaControls">
                ${this.isPlaying ? html`
                <paper-icon-button onclick="${() => this._pause()}" icon="av:pause"></paper-icon-button>` : html`
                <paper-icon-button onclick="${() => this._play()}" icon="av:play-arrow"></paper-icon-button>`}
            </div>
            
            <div id="songInfo">
                <div class="now-playing">
                    <div>
                        ${this.songTitle}
                    </div>
                    <div>
                        <span class="link">${this.artist}</span>
                        -
                        <span class="link">${this.album}</span>
                    </div>
                </div>
            </div>
            
            
            <div id="moreActions">
                <paper-icon-button icon="more-vert"></paper-icon-button>
            </div>

        `;
    }

    private _pause() {
        store.dispatch({
            type: AUDIO_PAUSE
        });
        this.updateProgress();
    }
    private _play() {
        store.dispatch({
            type: AUDIO_PLAY
        });
        this.updateProgress();
    }

    onKeyUp({ key }: KeyboardEvent) {
        switch (key) {

            case " ":
                this.isPlaying
                    ? this._pause()
                    : this._play();
                return;

        }
    }

    updateProgress() {
        const state = store.getState();
        const player = this.player;
        const progress = this.progressbar as any;
        const currentSong = state.currentSong;

        if (player && progress && state.currentSong) {
            progress.value = (1 / state.currentSong.meta.duration) * player.currentTime;
        }
    }

    _stateChanged(state: TStoreState) {
        if (state) {
            if (state.currentSong) {
                //this.song = state.currentSong;
                this.songTitle = state.currentSong.title
                this.album = state.currentSong.album;
                this.artist = state.currentSong.artist;
            } else {
                // Reset values
                this.songTitle = "";
                this.album = "";
                this.artist = "";
            }
        }
    }

}

window.customElements.define("lit-audio", LitAudio);


declare global {
    interface HTMLElementTagNameMap {
        "lit-audio": LitAudio;
    }
}