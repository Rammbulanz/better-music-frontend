import { LitElement, html, property } from "@polymer/lit-element";
import { AlbumResponse } from "../../../types/rest-results/AlbumResponse";
import { _default, card } from "../../shared-styles";
import { img } from "../lit-snippets";


class AlbumCard extends LitElement {

    @property({ type: Object })
    album: AlbumResponse

    protected render() {

        if (!this.album) {
            return html``;
        }

        return html`
            ${_default}
            ${card}
            <style>
                .card {
                    width: 200px;
                    max-width: 300px;               
                }

                #title {
                    display: block;
                    overflow: hidden;
                    white-space: nowrap;
                    font-size: 15px;
                    line-height: 18px;
                    font-weight: 400;
                    color: #212121;
                    text-decoration: none;
                    cursor: pointer;
                    outline: none;
                }
                #artist {
                    display: block;
                    overflow: hidden;
                    white-space: nowrap;
                    font-size: 13px;
                    line-height: 16px;
                    height: 16px;
                    font-weight: 400;
                    color: #616161;
                    margin-top: 4px;
                    text-decoration: none;
                    outline: none;
                }
                
                img {
                    width: 100%; 
                    height: auto;
                }
                
            </style>

            <div class="card card-shadow">
                ${img(`/btm/music/library/cover/album/${this.album.artist}/${this.album.title}`, "images/unknown-album.png", "picture")}
               <div class="details">
                  <a id="title">${this.album.title}</a>
                  <span id="artist">${this.album.artist}</span>
               </div>
            </div>

         `;
    }

}


window.customElements.define("album-card", AlbumCard);

declare global {
    interface HTMLElementTagNameMap {
        "album-card": AlbumCard;
    }
}