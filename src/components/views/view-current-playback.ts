import { Connected } from "../connected";
import { TStoreState } from "../../types/TStoreState";
import { html, property } from "@polymer/lit-element";
import { store } from "../../store";
import { AUDIO_PAUSE, AUDIO_PLAY } from "../../actions/audio";
import { _default, absoluteFullSize } from "../shared-styles";


export class ViewCurrentPlayback extends Connected {

    @property({ type: String })
    songId: string
    @property({ type: String })
    songTitle: string
    @property({ type: String })
    album: string
    @property({ type: String })
    artist: string
    @property({ type: Boolean })
    isPlaying: boolean

    public get player() {
        return store.getState().audio;
    }

    protected render() {
        return html`
            ${_default}
            ${absoluteFullSize}
            <style>
                :host {
                    width: 100%;
                    height: 100%;
                }
                :host {
                    display:flex;
                    flex-direction: column;
                }

                #coverWrapper {
                    flex-grow: 1;
                }

                #mediaControls {
                    display:flex;
                    justify-content: center;
                    align-items: center;
                }
            </style>

            <div id="coverWrapper">
                <img src="/btm/music/library/cover/song/${this.songId}" />
            </div>
            
            <div id="mediaControls">
                ${this.isPlaying ? html`
                <paper-icon-button onclick="${() => this._pause()}" icon="av:pause"></paper-icon-button>` : html`
                <paper-icon-button onclick="${() => this._play()}" icon="av:play-arrow"></paper-icon-button>`}
            </div>

        `;
    }

    private _pause() {
        store.dispatch({
            type: AUDIO_PAUSE
        });
    }
    private _play() {
        store.dispatch({
            type: AUDIO_PLAY
        });
    }

    _stateChanged(state: TStoreState) {
        if (state) {
            if (state.currentSong) {
                this.songId = state.currentSong.id;
                this.songTitle = state.currentSong.title;
            }
        }
    }

}

window.customElements.define("view-current-playback", ViewCurrentPlayback);



declare global {
    interface HTMLElementTagNameMap {
        "view-current-playback": ViewCurrentPlayback
    }
}