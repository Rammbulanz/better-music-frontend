import { html, LitElement, property, PropertyValues } from '@polymer/lit-element';
import { AlbumList } from '../../types/AlbumList';
import { _default, scrollbar, absoluteFullSize } from '../shared-styles';



class ViewAlbumCollection extends LitElement {

	public static get is() {
		return "view-album-collection";
	}

	@property({ type: String })
	public src: string

	@property({ type: Array })
	public albumList: AlbumList = [];

	protected update(changed: PropertyValues) {

		if (changed.has("src")) {
			this.loadAlbumList(this.src as string);
		}

		return super.update(changed);
	}

	protected render() {
		return html`
			${_default}
			${scrollbar}
			${absoluteFullSize}
			<style>
				album-card {
					float:left;
					padding: 10px;
				}
			</style>

			${this.albumList.filter(a => a).map(album => html`<album-card .album="${album}"></album-card>`)}
		`;
	}

	public async loadAlbumList(url: string) {
		if (!url) {
			return;
		}

		const res = await fetch(url);

		if (res.status == 200) {
			this.albumList = await res.json();
		} else {
			console.error(res.url, res.status, res.statusText);
		}

	}

}

window.customElements.define("view-album-collection", ViewAlbumCollection);
declare global {
	interface HTMLElementTagNameMap {
		"view-album-collection": ViewAlbumCollection;
	}
}