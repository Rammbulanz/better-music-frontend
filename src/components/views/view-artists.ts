import { html, LitElement, property, PropertyValues } from "@polymer/lit-element";
import { ArtistResponse } from "../../types/rest-results/ArtistResponse";
import { _default, absoluteFullSize, card, scrollbar } from "../shared-styles";


export class ViewArtists extends LitElement {

    static get is() {
        return "view-artists";
    }

	@property({ type: String })
	public src: string

    @property({ type: Object })
    public artists: Array<ArtistResponse> = [];
    @property({ type: Object })
    public seletedArtist: ArtistResponse;

	protected update(changed: PropertyValues) {

		if (changed.has("src")) {
			this.loadArtists(this.src as string);
		}

		return super.update(changed);
	}


    protected render() {
        return html`
            ${_default}
            ${absoluteFullSize}
            ${card}
            ${scrollbar}
            <style>
                :host {
                    display:flex;
                }
            </style>
           
            <div>
                <paper-listbox class="scrollContainer">
                    ${this.artists.map(artist => html`
                        <paper-item @click="${() => this.seletedArtist = artist}">
                            <paper-item-body>${artist.name}</paper-item-body>
                        </paper-item>
                    `)}
                </paper-listbox>
            </div>
            
         `;
    }

	public async loadArtists(url: string) {
		if (!url) {
			return;
		}

		const res = await fetch(url);

		if (res.status == 200) {
			this.artists = await res.json();
		} else {
			console.error(res.url, res.status, res.statusText);
		}

	}

}

customElements.define("view-artists", ViewArtists);


declare global {
    interface HTMLElementTagNameMap {
        "view-artists": ViewArtists;
    }
}