import { html } from "@polymer/lit-element";

export const _default = html`
   <style>
      .scrollContainer {
         overflow-y: auto;
      }
      iron-selector .iron-selected {
         background-color: var(--dark-theme-secondary-color);
      }
      a:hover {
         text-decoration: underline;
      }
   </style>
`;

export const absoluteFullSize = html`
    <style>
        :host {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
        }
    </style>
`;

export const scrollbar = html`
    <style>
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888; 
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
    </style>
`;

export const card = html`
    <style>
        /* Add shadows to create the "card" effect */
        .card {
            overflow: visible;
            white-space: nowrap;
            display: inline-block;
            position: relative;
            background-color: white;
            width: 0;
            margin: 8px;
            border-radius: 2px;
            font-weight: 300;
            vertical-align: top;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            cursor: pointer;
        }
        .card-shadow {
            box-shadow: 0 1px 4px 0 rgba(0,0,0,0.37);
            transition: box-shadow 0.28s cubic-bezier(0.4,0,0.2,1);
        }

        /* On mouse-over, add a deeper shadow */
        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        /* Add some padding inside the card container */
        .card .details {
            padding: 16px;
            position: relative;
        }
    </style>
`;

export const table = html`
        <style>
        
            .table {
                display:table;
            }            
            .table>* {
                display: table-row;
            }
            .table>*>* {
                display: table-cell;
            }
                        
            .table.table-border {
                border-collapse: collapse;
            }
            .table.table-border > * > * {
                padding: 15px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }
            
        </style>
    `;