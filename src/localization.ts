import i18next from 'i18next'
import Backend from 'i18next-xhr-backend'

export const locale = i18next
    .use(Backend)
    .init({
        backend: {
            loadPath: '/locales/{{ns}}_{{lng}}.json'
        },
        fallbackLng: "en",
        ns: ["common"]
    });

export const waitForLocale = new Promise(fulfill => {
    locale.on("loaded", fulfill);
});