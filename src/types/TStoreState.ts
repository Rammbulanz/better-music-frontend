import { Playlist } from "./Playlist";
import { AnyAction } from "redux";
import { Pages } from "./Pages";
import { SongResponse } from "./rest-results/SongResponse";


export type TStoreState = {
    audio
    currentSong: SongResponse
    currentPlaylist: Playlist
    view: string
    pages: Pages
    lastAction: AnyAction
}