import { SongResponse } from "./rest-results/SongResponse";

export type Playlist = {
    title: string
    current: number
    songs: Array<SongResponse>
}