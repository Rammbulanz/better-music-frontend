import { TemplateResult } from "lit-html";

export type Pages = {
   [pageName: string]: TemplateResult
}