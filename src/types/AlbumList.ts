import { AlbumResponse } from "./rest-results/AlbumResponse";

export type AlbumList = Array<AlbumResponse>