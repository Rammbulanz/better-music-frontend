export type CoverCollection = {
   [url: string]: {
      type: string
      data: any
   }
}