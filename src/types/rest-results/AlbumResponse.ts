export type AlbumResponse = {
    title: string
    artist: string
    /**
     * Collection of IDs
     */
    songs?: Array<string>
}