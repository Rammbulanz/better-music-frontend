export type SongResponse = {
    id: string
    title: string
    artist: string
    album: string
    meta: {
        duration: number
        type: string
    }
}