import { store } from "../store";
import { SET_PROFILE } from "../actions/profile";

export const setProfile = (name: string) => store.dispatch({
   type: SET_PROFILE,
   name
});