import { Playlist } from "../types/Playlist";
import { store } from "../store";
import { SET_CURRENT_PLAYLIST, UNSET_CURRENT_PLAYLIST } from "../actions/currentPlaylist";

export const setCurrentPlaylist = (playlist: Playlist) => store.dispatch({
    type: SET_CURRENT_PLAYLIST,
    playlist
});

export const unsetCurrentPlaylist = () => store.dispatch({
    type: UNSET_CURRENT_PLAYLIST
});