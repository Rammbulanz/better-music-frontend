import { store } from "../store";
import { SET_CURRENT_SONG, UNSET_CURRENT_SONG } from "../actions/currentSong";
import { SongResponse } from "../types/rest-results/SongResponse";

export const setCurrentSong = (song: SongResponse) => store.dispatch({
    type: SET_CURRENT_SONG,
    song
});

export const unsetCurrentSong = () => store.dispatch({
    type: UNSET_CURRENT_SONG
})